# "Urbanity as a process and the role of relative network properties - A case study from the Early Iron Age"

## Metadata:

### Autors:

- Oliver Nakoinz <oliver.nakoinz@ufg.uni-kiel.de>
- Michael Bilger <sofnod@googlemail.com>
- David Matzig <stu209960@mail.uni-kiel.de>

(all Institute of Pre- and Protohistoric Archaeology of Kiel University, Johanna-Mestorf-Straße 2-6, D - 24118 Kiel)

### Volume

- Francesca Fulminante, John William Hanson, Scott G. Ortman, Luis M. A. Bettencourt
- Where Do Cities Come From and Where Are They Going To? Modelling Past and Present Agglomerations to Understand Urban Ways of Life
- Frontiers

<!-- 
### Links

[https://www.frontiersin.org/research-topics/7460](https://www.frontiersin.org/research-topics/7460)
[http://www.frontiersin.org/about/author-guidelines](http://www.frontiersin.org/about/author-guidelines)
[http://www.frontiersin.org/about/PublishingFees](http://www.frontiersin.org/about/PublishingFees)

https://toreopsahl.com/2009/02/20/betweenness-in-weighted-networks/

https://www.frontiersin.org/about/author-guidelines
--> 

### Abstract:

Urbanity as a process and the role of relative network properties - A case study from the Early Iron Age

Abstract:
A manifold of concepts of urbanity have been discussed in the past and many criteria of towns have been developed. These criteria, e. g.  size, population,legal aspects, way of life, structural and functional approaches are insufficient, because they cover just a part of the phenomenon and because they partly use fixed and arbitrary thresholds. We turn to an understanding of urbanity as a process which fills and shapes the scenery of the buildings and people. In this sense we understand urbanity as a process of adaption to changed requirements or contexts in a complex settlement system, triggered by size, attracted by exemplary solutions and characterized by the emergence of new structures.

This approach considers many different aspects of urbanity among which network approaches, being in the focus of this article, play a significant role. Interaction in network is one of the main mechanisms of adapting a settlement with a certain degree of complexity to new facts, conditions and requirements.

With this paper we address the issues of relativity in the urbanity process. As adaption process, urbanity is relative in the sense, that other places may have gained better or worse adaption. Concerning networks this means that the state of the development of network centrality depends on the centrality of the other places and further more, that centrality itself depend on the centrality of other places. The first is mapped perfectly by all network centrality measures which allow to compare the different scores while the second is addressed by eigenvector centrality in particular.

When dealing with network centrality measures we have to be aware that the centrality indices applied to an unweighted network results in structural centrality potential and not in actual centrality. For gaining the actual centrality networks weighted with the flow of commodities, ideas or people, representing the actual interaction between the nodes are required. Geographical networks where each node is connected to the natural neighbours rarely provide unexpected results. The results mainly show edge effects having high betweenness scores in the centre and low ones at peripheral locations. Taking the difference of weighted and unweighted network centrality scores as actual centrality highlights the real central nodes and connects to the concept of relative centrality mentioned above.

This paper applies this concept to the Princely Seats of the Early Iron Age with a special focus on the Heuneburg. A very  limited part of the material culture is used to represent similarities and interaction between the different nodes. For this purpose we use fibulae which allow rather good dating and hence ensure a narrow time slice for the network analysis. Due to limitations in the availability of data we focus on a Princely Seat only network and a network of one Princely Seat and its vicinity.

This paper is intended to contribute to the problem of addressing the rather complex issue of urbanity using rather simple approaches such as network analysis. We pay attention to a tight integration of theory and method as well as to certain conceptual issues. 


## Repository content

This repository contains:

### Data

- Data used for this case study:
    - Princly seats (Fürstensitze) of the early Iron Age:
        - 2data/fuerstensitze.csv
    - Fibulae from the Early Iron Age (completed data from shkr data base)
        - 2data/fibulae2018_had1.csv
        - 2data/fibulae2018_had2.csv
        - 2data/fibulae2018_had3.csv
        - 2data/fibulae2018_had1_ref.csv
        - 2data/fibulae2018_had2_ref.csv
        - 2data/fibulae2018_had3_ref.csv
        - 2data/fibulae2018.csv
- List of fibulae types for different periods:
    - 2data/had1.txt
    - 2data/had2.txt
    - 2data/had3.txt
- SHKR artefact thesaurus including fibulae typology:
    - 2data/thes_d.csv
- Geodata
    - 3geodata/srtm.grd
    - SRTM data downloaded and preprocessed using raster package.
- Simulated places
    - 2data/nodes.csv
    - 2data/nodes_co.csv
    - 2data/nodes_sf.csv

### Scripts

This project uses Rmarkdown (.rmd) files which contain R code and explainations

- 1script/1download_shkr_fibulae_v3.rmd
    - This script is included just for the sake of transparance because this case study uses a snapshot of the data to ensure reproducibility. The data downloades and prepared by this script are included in the repository.
- 1script/2placesim_v02.rmd
    - This script simulates settlement locations to ensure a distribution not biased by local research conditions.
- 1script/3analyse_v05.rmd
    - This is the actual analysis and visualisation script.




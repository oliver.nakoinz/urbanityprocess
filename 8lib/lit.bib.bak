% Encoding: UTF-8

@Article{nakoinz_101,
  Title                    = {Three Methods for Detecting Past Groupings: Cultural Space and Group Identity},
  Author                   = {Cormier, Laurie Tremblay and Nakoinz, Oliver and  Popa, Cătălin Nicolae},
  Journal                  = {Journal of Archaeological Method and Theory},
  Year                     = {2017},
  Pages                    = {xxx},
  Volume                   = {xxx},
  Abstract                 = {yyy},
  File                     = {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_101.pdf:PDF},
  Owner                    = {Oliver Nakoinz},
  Url                      = {https://doi.org/10.1007/s10816-017-9350-2}
}

@InBook{nakoinz_108,
  chapter =   {Quantifying Iron Age urbanism},
  pages =     {87--95},
  title =     {Delicate urbanism in context: pre-Roman German urbanism (The DAAD Cambridge Symposium)},
  publisher = {McDonald Institute},
  year =      {2017},
  author =    {Nakoinz, Oliver},
  editor =    {Simon Stoddart},
  series =    {McDonald Institute Monographs},
  address =   {Cambridge},
  comment =   {978-1-902937-83-0},
  file =      {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_108.pdf:PDF},
  keywords =  {Eisenzeit, Urbanität, Stadt, Heuneburg, ABM, Komplexität},
  url =       {https://www.arch.cam.ac.uk/iron_age/Stoddart_2017.pdf}
}

@Article{nakoinz_102,
  author =   {Nakoinz, Oliver},
  title =    {Kollektive und Netzwerke in der Archäologie und Altertumswissenschaft. Eine Einführung},
  journal =  {Zeitschrift für Kultur- und Kollektivwissenschaft},
  year =     {2017},
  volume =   {3},
  number =   {2},
  pages =    {7--17},
  abstract = {yyy},
  file =     {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_102.pdf:PDF},
  keywords = {Kultur, Kollektiv, Netzwerk, Eisenzeit},
  owner =    {Oliver Nakoinz},
  url =      {http://www.transcript-verlag.de/978-3-8376-3823-3/Zeitschrift-fuer-Kultur-und-Kollektivwissenschaft}
}

@Book{nakoinz_102-103,
  title =     {Zeitschrift für Kultur- und Kollektivwissenschaft Jg. 3, Heft 2/2017, [Kollektive und Netzwerke in der Archäologie und Altertumswissenschaft},
  publisher = {Transcript},
  year =      {2017},
  editor =    {Nakoinz, Oliver},
  volume =    {3},
  number =    {2},
  series =    {Zeitschrift für Kultur- und Kollektivwissenschaft},
  address =   {xxx},
  keywords =  {Kultur, Kollektiv, Netzwerk},
  url =       {http://www.transcript-verlag.de/978-3-8376-3823-3/Zeitschrift-fuer-Kultur-und-Kollektivwissenschaft}
}

@Article{nakoinz_103,
  author =   {Nakoinz, Oliver},
  title =    {Kollektive und Netzwerke als komplementäre Ansätze in der Eisenzeitforschung},
  journal =  {Zeitschrift für Kultur- und Kollektivwissenschaft},
  year =     {2017},
  volume =   {3},
  number =   {2},
  pages =    {61--82},
  abstract = {yyy},
  file =     {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_103.pdf:PDF},
  keywords = {Kultur, Kollektiv, Netzwerk, Eisenzeit},
  owner =    {Oliver Nakoinz},
  url =      {http://www.transcript-verlag.de/978-3-8376-3823-3/Zeitschrift-fuer-Kultur-und-Kollektivwissenschaft}
}

@InBook{nakoinz_080,
  chapter   = {Models of Centrality},
  pages     = {217--223},
  title     = {Landscape Archaeology. Conference (LAC 2012)},
  publisher = {Topoi},
  year      = {2012},
  author    = {Nakoinz, Oliver},
  editor    = {Wiebke Bebermeier and Robert Hebenstreit and Elke Kaiser and Jan Krause},
  volume    = {3},
  series    = {eTopoi Special Volume},
  address   = {Berlin},
  file                     = {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_80.pdf:PDF},
  note      = {xxx},
  url       = {xxx},
}

@Article{Grano1973,
  author  = {Granovetter, Mark},
  title   = {The strenght of weak ties},
  journal = {American Journal of Sociology},
  year    = {1973},
  volume  = {78},
  pages   = {1360--80},
}

@Book{knappett_2014,
  title     = {An archaeology of interaction: network perspectives on material culture and society},
  publisher = {Oxford University Press},
  year      = {2014},
  author    = {Knappett, Carl},
  place     = {New York},
}

@Article{nakoinz_112,
  author         = {Knitter, Daniel and Nakoinz, Oliver},
  title          = {The Relative Concentration of Interaction—A Proposal for an Integrated Understanding of Centrality and Central Places},
  journal        = {Land},
  year           = {2018},
  volume         = {7},
  number         = {3},
  issn           = {2073-445X},
  abstract       = {The importance of a place can be assessed via an analysis of its centrality. However, although central place research has a long history, there is no generally accepted theoretical base, leading to continuous debates about the core elements of centrality and those features that ultimately constitute the centrality of a place. We propose a generalized definition that understands centrality as the relative concentration of interaction. Using this definition, we are able to integrate various social, cultural, and natural aspects in the analysis of a central place and its landscape setting. We present a semi-quantitative method to assess the actual and potential centrality and that enables us (a) to draw conclusions about the type and characteristics of central places, (b) to investigate their development throughout time, and (c) to compare them to each other. We sketch the application of the method using two exemplary sites: the Iron Age site Heuneburg and the Roman palace Felix Romuliana},
  article-number = {86},
  doi            = {10.3390/land7030086},
  url            = {http://www.mdpi.com/2073-445X/7/3/86},
}

@Book{nakoinz_topoi,
  title     = {Zentralität - Theorie, Methoden und Fallbeispiele zur Analyse zentraler Orte},
  publisher = {Topoi},
  year      = {in print},
  author    = {Oliver Nakoinz},
  series    = {Topoi Edition},
  address   = {Berlin},
}

@InBook{Nakoinz2013b,
  chapter   = {Zentralorte in parallelen Raumstrukturen},
  pages     = {83-103},
  title     = {Parallele Raumstrukturen.},
  publisher = {Topoi},
  year      = {2013},
  author    = {Oliver Nakoinz},
  editor    = {Svend Hansen and Michael Meier},
  volume    = {16},
  series    = {Topoi Berlin Studies of the Ancient World},
  address   = {Berlin},
}

@InBook{Nakoinz2010,
  chapter   = {Concepts of Central Place Research in Archaeology},
  pages     = {251-264},
  title     = {Landscapes and Human Development: The Contribution of European Archaeology. Proceedings of the International Workshop 'Socio-environmental dynamics over the last 12,000 years: the creation of landscapes (1st-4th April 2009)'},
  publisher = {Habelt},
  year      = {2010},
  author    = {Oliver Nakoinz},
  editor    = {Kiel Graduate School 'Human Development in Landscapes'},
  address   = {Bonn},
}

@InBook{nakoinz_092,
  chapter   = {Fingerprinting Iron Age Communities in South-West-Germany and an Integrative Theory of Culture},
  pages     = {187-199},
  title     = {Fingerprintig the Iron Age},
  publisher = {Oxbow},
  year      = {2014},
  author    = {Nakoinz, Oliver},
  editor    = {Catalin Popa and Simon Stoddart},
  address   = {Oxford},
  note      = {xxx},
  file      = {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_92.pdf:PDF},
  url       = {https://www.researchgate.net/publication/301282110_Fingerprinting_Iron_Age_Communities_in_South-West-Germany_and_an_Integrative_Theory_of_Culture},
}

@Article{nakoinz_089,
  author   = {Nakoinz, Oliver},
  title    = {Models of Interaction and Economical Archaeology},
  journal  = {Metalla},
  year     = {2013},
  volume   = {20},
  number   = {2},
  pages    = {107--115},
  abstract = {yyy},
  file     = {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_89.pdf:PDF},
  owner    = {Oliver Nakoinz},
  url      = {xxx},
}

@Article{nakoinz_090,
  author   = {Nakoinz, Oliver},
  title    = {Räumliche Interaktionsmodelle},
  journal  = {Prähist. Zeitschr.},
  year     = {2013},
  volume   = {88},
  pages    = {226--257},
  abstract = {yyy},
  file     = {:arch/arch_01/Literatur/nakoinz/nakoinz_schr_90.pdf:PDF},
  owner    = {Oliver Nakoinz},
  url      = {https://www.degruyter.com/view/j/pz.2013.88.issue-1-2/pz-2013-0008/pz-2013-0008.xml},
}

@Book{nakoinz_084,
  title     = {Archäologische Kulturgeographie der ältereisenzeitlichen Zentralorte Südwestdeutschlands},
  publisher = {Habelt},
  year      = {2013},
  author    = {Nakoinz, Oliver},
  volume    = {224},
  series    = {Universitätsforsch. Prähist. Arch.},
  address   = {Bonn},
}

@Book{nakoinz_097,
  title     = {Modelling Human Behaviour in Landscapes - Basic concepts and modelling elements},
  publisher = {Springer},
  year      = {2016},
  author    = {Nakoinz, Oliver and Knitter, Daniel},
  volume    = {1},
  series    = {Quantitative Archaeology and Archaeological Modelling},
  address   = {New York},
  url       = {http://www.springer.com/de/book/9783319295367},
}

@Book{Haggett1969,
  title     = {Network Analysis in Geography},
  publisher = {Arnold},
  year      = {1969},
  author    = {P. Haggett and R. J. Chorley},
  address   = {London},
  owner     = {S.A.},
  timestamp = {2014.07.29},
}

@Article{Sindbaek2007,
  author  = {S. Sindbæk},
  title   = {Networks and nodal points: The emergence of towns in early Viking Age Scandinavia},
  journal = {Antiquity},
  year    = {2007},
  volume  = {81},
  pages   = {119-132},
}

@Article{Barthelemy2014,
  author   = {Marc Barthelemy},
  title    = {Discussion: Social and spatial networks},
  journal  = {Les nouvelles de l'archéologie},
  year     = {2014},
  volume   = {135},
  pages    = {51-61},
  doi      = {10.4000/nda.2374},
  keywords = {network, social network, spatial network},
  url      = {http://nda.revues.org/2374},
}

@Article{Brughmans2010,
  author    = {BRUGHMANS, TOM},
  title     = {CONNECTING THE DOTS: TOWARDS ARCHAEOLOGICAL NETWORK ANALYSIS},
  journal   = {Oxford Journal of Archaeology},
  year      = {2010},
  volume    = {29},
  number    = {3},
  pages     = {277--303},
  issn      = {1468-0092},
  doi       = {10.1111/j.1468-0092.2010.00349.x},
  publisher = {Blackwell Publishing Ltd},
  url       = {http://dx.doi.org/10.1111/j.1468-0092.2010.00349.x},
}

@Article{Collar2015,
  author  = {A. Collar, F. Coeard, T. Brughmans, B. Mills},
  title   = {Networks in Archaeology: Phenomena, Abstraction, Representation},
  journal = {Journal of Archaeological Method and Theory},
  year    = {2015},
  volume  = {22},
  pages   = {1-32},
}

@Article{Fulminante2014,
  author  = {F. Fulminante},
  title   = {The network approach: tool or paradigm?},
  journal = {Archaeological Review from Cambridge},
  year    = {2014},
  number  = {29},
  pages   = {167-178},
}

@Book{Brandes2005,
  title     = {Network Analysis},
  publisher = {Springer},
  year      = {2005},
  editor    = {U. Brandes and T. Erlebach},
  address   = {Berlin u. Heidelberg},
  owner     = {S.A.},
  timestamp = {2014.07.19},
}

@Book{Scott2000,
  title     = {Social Network Analysis},
  publisher = {SAGE},
  year      = {2000},
  author    = {J. Scott},
  address   = {London u.a.},
  owner     = {S.A.},
  timestamp = {2014.08.03},
}

@Book{Taaffe1973,
  title         = {Geography of transportation [by] Edward J. Taaffe [and] Howard L. Gauthier, Jr},
  publisher     = {Prentice-Hall Englewood Cliffs, N.J},
  year          = {1973},
  author        = {Taaffe, Edward J. and Gauthier, Howard L,},
  isbn          = {0133513955 0133513874},
  catalogue-url = { https://nla.gov.au/nla.cat-vn1954473 },
  language      = {English},
  life-dates    = { 1973 - },
  pages         = {xiv, 226 p.},
  subjects      = { Transportation geography. },
  type          = {Book},
}

@Book{wehner,
  title     = {Artefakt-Netzwerke im östlichen Mitteleuropa an der Schwelle zum hohen Mittelalter: Zur Quantifizierung, Visualisierung und Beschaffenheit überregionaler Kommunikations- und Austauschbeziehungen},
  publisher = {Habelt},
  year      = {in press},
  author    = {Donat Wehner},
  series    = {Universitätsforsch. prähist. Arch.},
  address   = {Bonn},
  owner     = {fon},
  timestamp = {2018.02.02},
}

@InCollection{Wilson1978,
  author    = {Wilson, A. G.},
  title     = {Spatial Interaction and Settlement Structure: Towards an Explicit Central Place Theory},
  booktitle = {Spatial Interaction Theory and Planning Models},
  year      = {1978},
  editor    = {Karlqvist, S. and Lundqvist, L. and Snickars, F. and Weibull, J. W.},
  address   = {Amsterdam},
  owner     = {fon},
  timestamp = {2018.01.30},
}

@Book{kohler_2000,
  title      = {Dynamics in human and primate societies: agent-based modeling of social and spatial processes},
  publisher  = {Oxford University Press},
  year       = {2000},
  author     = {Kohler, Timothy A and Gumerman, George J},
  address    = {New York},
  isbn       = {9780195351194 9786610833276 9780195131673},
  note       = {OCLC: 433134206},
  abstract   = {Preface. 1. Putting Social Sciences Together Again: An Introduction to the Volume, Timothy A. Kohler. 2. Nonlinear and Synthetic Models for Primate Societies, Irenaeus J.A. te Boekhorst and Charlotte K. Hemelrijk. 3. The Evolution of Cooperation in an Ecological Context: An Agent-Based Model, John W. Pepper and Barbara B. Smuts. 4. Evolution of Interference, Brian Skyrms. 5. Trajectories to Complexity in Artificial Societies: Rationality, Belief, and Emotions, Jim E. Doran. 6. MAGICAL Computer Simulation of Mesolithic Foraging, Mark Winter Lake. 7. Be There Then: A Modeling Approach to Settle.},
  language   = {English},
  shorttitle = {Dynamics in human and primate societies},
  url        = {http://public.eblib.com/choice/publicfullrecord.aspx?p=430465},
  urldate    = {2018-11-11TZ},
}

@Book{kohler_2007,
  title     = {The model-based archaeology of socionatural systems},
  publisher = {School for Advanced Research Press},
  year      = {2007},
  editor    = {Kohler, Timothy A. and Leeuw, Sander Ernst van der},
  address   = {Santa Fe, N.M},
  edition   = {1st ed},
  isbn      = {9781930618879},
  keywords  = {Social archaeology, Ethnoarchaeology, Human ecology, History, Archaeology, Computer simulation, Archaeology, Methodology},
}

@Book{christaller1933,
  title     = {Die zentralen {Orte} in {Süddeutschland} - {Eine} ökonomisch-geographische {Untersuchung} über die {Gesetzmäßigkeiten} der {Verbreitung} und {Entwicklung} der {Siedlungen} mit städtischer {Funktion}},
  publisher = {Gustav Fischer},
  year      = {1933},
  author    = {Christaller, Walter},
  address   = {Jena},
}

@InCollection{denecke1972,
  author    = {Denecke, Dietrich},
  title     = {Der geographische {Stadtbegriff} und die räumlich-funktionale {Betrachtungsweise} bei {Siedlungstypen} mit zentraler {Bedeutung} in {Anwendung} auf historische {Siedlungsepochen}},
  booktitle = {Vor- und {Frühformen} der europäischen {Stadt} im {Mittelalter}: {Bericht} über ein {Symposium} in {Reinhausen} bei {Göttingen} in der {Zeit} vom 18. bis 24. {April} 1972},
  publisher = {Vandenhoeck \& Ruprecht},
  year      = {1972},
  editor    = {Jankuhn, Herbert and Schlesinger, Walter and Steuer, Heiko},
  series    = {Abhandlungen der {Akademie} der {Wissenschaften} in {Göttingen} 3},
  pages     = {33--55},
  address   = {Göttingen},
  language  = {ger},
}

@Article{fernandez-goetz_krausse_2013,
  author    = {Fernández-Götz, Manuel and Krausse, Dirk},
  title     = {Rethinking Early Iron Age urbanisation in Central Europe: the Heuneburg site and its archaeological environment},
  journal   = {Antiquity},
  year      = {2013},
  volume    = {87},
  number    = {336},
  pages     = {473--487},
  doi       = {10.1017/S0003598X00049073},
  publisher = {Cambridge University Press},
}

@Article{freeman1978,
  author   = {Freeman, Linton C.},
  title    = {Centrality in social networks conceptual clarification},
  journal  = {Social Networks},
  year     = {1978},
  volume   = {1},
  number   = {3},
  pages    = {215--239},
  issn     = {0378-8733},
  abstract = {The intuitive background for measures of structural centrality in social networks is reviewed and existing measures are evaluated in terms of their consistency with intuitions and their interpretability. Three distinct intuitive conceptions of centrality are uncovered and existing measures are refined to embody these conceptions. Three measures are developed for each concept, one absolute and one relative measure of the centrality of positions in a network, and one reflecting the degree of centralization of the entire network. The implications of these measures for the experimental study of small groups is examined.},
  doi      = {10.1016/0378-8733(78)90021-7},
  url      = {http://www.sciencedirect.com/science/article/pii/0378873378900217},
  urldate  = {2018-06-27},
}

@Article{freeman1979,
  author     = {Freeman, Linton C and Roeder, Douglas and Mulholland, Robert R},
  title      = {Centrality in social networks: ii. experimental results},
  journal    = {Social Networks},
  year       = {1979},
  volume     = {2},
  number     = {2},
  pages      = {119--141},
  issn       = {0378-8733},
  abstract   = {Three competing hypotheses about structural centrality are explored by means of a replication of the early MIT experiments on communication structure and group problem-solving. It is shown that although two of the three kinds of measures of centrality have a demonstrable effect on individual responses and group processes, the classic measure of centrality based on distance is unrelated to any experimental variable. A suggestion is made that the positive results provided by distance-based centrality in earlier experiments is an artifact of the particular structures chosen for experimentation.},
  doi        = {10.1016/0378-8733(79)90002-9},
  shorttitle = {Centrality in social networks},
  url        = {http://www.sciencedirect.com/science/article/pii/0378873379900029},
  urldate    = {2018-06-27},
}

@Book{freeman2004,
  title      = {The development of social network analysis: a study in the sociology of science},
  publisher  = {Empirical Press ; BookSurge},
  year       = {2004},
  author     = {Freeman, Linton C.},
  address    = {Vancouver, BC : North Charleston, S.C},
  isbn       = {978-1-59457-714-7},
  language   = {en},
  shorttitle = {The development of social network analysis},
}

@Article{gringmuth-dallmer1996,
  author  = {Gringmuth-Dallmer, Eike},
  title   = {Kulturlandschaftsmuster und {Siedlungssysteme}},
  journal = {Siedlungsforschung},
  year    = {1996},
  volume  = {14},
  pages   = {7--31},
}

@Article{smith1989,
  author  = {Smith, M. E.},
  title   = {Cities, towns, and urbanism: comment on Sanders and Webster.},
  journal = {American Anthropologist},
  year    = {1989},
  volume  = {91},
  pages   = {454--61},
}

@Article{wirth1938,
  author  = {Wirth, L.},
  title   = {Urbanism as a Way of Life.},
  journal = {American Journal of Sociology},
  year    = {1938},
  volume  = {44},
  pages   = {1--24},
}

@Book{hasse1880,
  title     = {Das Schleswiger Stadtrecht: Untersuchungen zur Dänischen Rechtsgeschichte},
  publisher = {Lipsius \& Tischer},
  year      = {1880},
  author    = {Hasse, P.},
  address   = {Kiel},
}

@InBook{gans1962,
  chapter   = {Urbanism and Suburbanism as Ways of Life: A Reevaluation of Definitions.},
  pages     = {625--48},
  title     = {Human Behavior and Social Process},
  publisher = {Rose, A. M.},
  year      = {1962},
  author    = {Gans, H. J.},
  editor    = {Rose, A. M.},
  address   = {Boston},
}

@Electronic{deuskar2015,
  author       = {Deuskar},
  year         = {2015},
  title        = {What does 'urban' mean?},
  howpublished = {Online Blog},
  url          = {http://blogs.worldbank.org/sustainablecities/what-does-urban-mean},
}

@Misc{Eurostat,
  author       = {Eurostat},
  title        = {Urban-rural typology. Eurostat Statistic Explained},
  howpublished = {Online},
  url          = {http://ec.europa.eu/eurostat/statistics-explained/index.php/Urban-rural_typology},
}

@Misc{demographia2015,
  author       = {Demographia},
  title        = {Demographia World Urban Areas},
  howpublished = {Online},
  year         = {2015},
  url          = {http://www.urbangateway.org/system/files/documents/urbangateway/db-worldua.pdf},
}

@Article{Filet2016,
  author   = {Filet, Clara},
  title    = {An Attempt to Estimate the Impact of the Spread of Economic Flows on Latenian Urbanization1},
  journal  = {Frontiers in Digital Humanities},
  year     = {2017},
  volume   = {3},
  pages    = {10},
  issn     = {2297-2668},
  abstract = {Over a relatively short period between the end of the 4th and the middle of the 1st century BC, an unprecedented process of urbanisation developed in non-Mediterranean Europe. Among all the factors contributing to the rise of the first agglomerations possessing urban characteristics in this area, this article focuses on the role of commercial interactions. The ability of settlements to interact within the trade network is approached by modelling of interactions. The aim is to provide new material to estimate the extent to which this factor could have impacted the known hierarchy of settlements on the one hand, and its role in the development of the Latenian urbanisation process on the other hand.},
  doi      = {10.3389/fdigh.2016.00010},
  url      = {http://journal.frontiersin.org/article/10.3389/fdigh.2016.00010},
}

@Inbook{Raab2010,
author="Raab, J{\"o}rg",
editor="Stegbauer, Christian
and H{\"a}u{\ss}ling, Roger",
title="Der „Harvard Breakthrough``",
bookTitle="Handbuch Netzwerkforschung",
year="2010",
publisher="VS Verlag f{\"u}r Sozialwissenschaften",
address="Wiesbaden",
pages="29--37",
abstract="W{\"a}hrend der letzten drei{\ss}ig Jahre wurde die empirische sozialwissenschaftliche Forschung durch Fragebogenerhebungen auf Basis der Zufallsauswahl dominiert. Jedoch gleicht die Zufallsauswahl von Individuen, wie es normalerweise praktiziert wird, einem soziologischen Fleischwolf, der das Individuum aus seinem sozialen Kontext rei{\ss}t und damit garantiert, dass niemand innerhalb der Studie mit jemand anderem interagiert. Dieses Vorgehen ist mit dem eines Biologen vergleichbar, der seine Versuchstiere in eine Hamburgermaschine stopft, um danach jede hundertste Zelle unter dem Mikroskop zu betrachten. Anatomie und Physiologie gehen verloren, Struktur und Funktion verschwinden, was bleibt ist Zellbiologie{\ldots}.Wenn es unser Ziel ist, das Verhalten von Menschen zu verstehen und es nicht lediglich festzuhalten, dann m{\"u}ssen wir uns mit den folgenden sozialen Ph{\"a}nomenen besch{\"a}ftigen und dar{\"u}ber Wissen generieren: mit Prim{\"a}rgruppen, mit Nachbarschaften, Organisationen, sozialen Kreisen und Gemeinschaften sowie mit Interaktion, Kommunikation, Rollenerwartungen und sozialer Kontrolle`` (Allan Barton 1968 zitiert in Freeman 2004: 1).",
isbn="978-3-531-92575-2",
doi="10.1007/978-3-531-92575-2_4",
url="https://doi.org/10.1007/978-3-531-92575-2_4"
}

@book{krausse2016,
author = {Krausse, Dirk and Fernandez-Gotz, Manuel and Hansen, Leif and Kretschmer, Inga},
year = {2016},
month = {01},
pages = {},
title = {The Heuneburg and the Early Iron Age Princely Seats: First Towns North of the Alps}
}

@Book{Batty2005,
  title     = {Cities and Complexity: Understanding Cities with Cellular Automata, Agent-Based Models, and Fractals.},
  publisher = {The MIT Press},
  year      = {2005},
  author    = {M. Batty},
  address   = {Cambridge, MA},
}

@InCollection{Brun1988,
  author    = {P. Brun},
  title     = {Les 'résidences princières' comme centres territoriaux: élements de vérification.},
  booktitle = {Les princes celtes et la Méditerranée. Actes du colloque Paris 1987},
  publisher = {Rencontres de l'école du Louvre},
  year      = {1988},
  editor    = {J. P. Mohen, A. Duval, C.Eluère},
  series    = {Rencontres de l'école du Louvre},
  pages     = {128-143},
}

@Article{Brun_Chaume2013,
  author  = {P. Brun and B. Chaume},
  title   = {Une éphémère tentative d’urbanisation en Europe centre-occidentale durant les vi e et v e siècles av. J.‐C.?},
  journal = {Bulletin de la Société Préhistorique Française},
  year    = {2013},
  volume  = {110},
  number  = {2},
  pages   = {319-349},
}

@Article{Camagni_Salone1993,
  author  = {Camagni, R. P. and Salone, C.},
  title   = {Network Urban Structures in Northern Italy: Elements for a Theoretical Framework.},
  journal = {Urban Studies},
  year    = {1993},
}

@Book{vonBertalanffy_1968,
  title     = {General System Theory: Foundations, Development, Applications},
  publisher = {George Braziller},
  year      = {1968},
  author    = {L. von Bertalanffy},
}

@Article{Gell-Mann1995,
  author  = {M. Gell-Mann},
  title   = {What is Complexity?},
  journal = {Complexity},
  year    = {1995},
  volume  = {1},
  pages   = {16--19},
}

@Article{@Feinmann2011,
  author  = {G.M. Feinmann},
  title   = {Size, Complexity, and Organizational Variation: A Comparative Approach},
  journal = {Cross-Cultural Research},
  year    = {2011},
  volume  = {45},
  pages   = {37--58},
}

@Article{MacSweeney2004,
  author  = {N. MacSweeney},
  title   = {Social Complexity and Population: A Study in the Early Bronze Age Aegean},
  journal = {Papers of the Institute of Archaeology},
  year    = {2004},
  volume  = {15},
  pages   = {53--66},
}

@Book{Mansfeld1973,
  title     = {Die Fibeln der Heuneburg 1950-1970. Ein Beitrag zur Geschichte der Späthallstattfibel.},
  publisher = {de Gruyter},
  year      = {1973},
  author    = {Günter Mansfeld},
  volume    = {33},
  series    = {Römisch-Germanische Forschungen},
  note      = {Heuneburgstudien II},
}

@InBook{nakoinz_113,
  chapter   = {Models and Modelling in Archaeology.},
  pages     = {101-112},
  title     = {Models and Modelling between Digital and Humanities -- A Multidisciplinary Perspective},
  publisher = {GESIS – Leibniz-Institut für Sozialwissenschaften},
  year      = {2018},
  author    = {Oliver Nakoinz},
  editor    = {Arianna Ciula and Øyvind Eide and Cristina Marras and Patrick Sahle},
  volume    = {31},
  series    = {Historical Social Research Supplement},
  isbn      = {0172-6404, 0936-6784},
}

@Article{Arponen_inprint,
  author  = {Arponen, V.P.J. and Müller, Johannes and Hofmann, Robert and Furholt, Martin and Ribeiro, Artur and Horn, Christian and Hinz, Martin},
  title   = {Using the Capability Approach to Conceptualise Inequality in Archaeology: the Case of the Late Neolithic Bosnian Site Okoliste c. 5200-4600 BCE.},
  journal = {Journal of Archaeological Method and Theory},
  year    = {in print},
  url     = {http://link.springer.com/article/10.1007/s10816-015-9252-0/fulltext.html},
}

@Article{Alberti2014,
  author  = {Alberti, Gianmarco},
  title   = {Modeling Group Size and Scalar Stress by Logistic Regression from an Archaeological Perspective},
  journal = {Plos One},
  year    = {2014},
  volume  = {9},
  number  = {3},
  pages   = {15},
  month   = mar,
}

@Book{vdBoogaart2013,
  title     = {Analyzing Compositional Data with R},
  publisher = {Springer-Verlag Berlin Heidelberg},
  year      = {2013},
  author    = {van den Boogaart, K.G. and Tolosana-Delgado, R.},
}

@Book{Krausse2008,
  title     = {Frühe Zentralisierungs- und Urbanisierungsprozesse. Zur Genese und Entwicklung frühkeltischer Fürstensitze und ihres territorialen Umlandes.},
  publisher = {Konrad Theiss Verlag Stuttgart},
  year      = {2008},
  editor    = {Krausse, Dirk},
}

@Book{@Stoddart2017,
  title     = {Delicate Urbanism in Context: Settlement Nucleation in pre-Roman Germany},
  publisher = {Cambridge},
  year      = {2017},
  editor    = {Stoddart, Simon},
}

@InCollection{@Kimmig1969,
  author    = {Kimmig, W.},
  title     = {Zum Problem späthallstättischer Adelssitze.},
  booktitle = {Siedlung, Burg und Stadt. Studien zu ihren Anfängen [Festschrift P. Grimm]},
  year      = {1969},
  editor    = {Otto, K.-H. and Hermann, J.},
  pages     = {95--113},
}

@Misc{knitter_2017,
  author = {Daniel Knitter},
  title  = {{moin -- Modeling Interactions using gravity and entropy maximizing approaches}},
  month  = aug,
  year   = {2017},
  doi    = {10.5281/zenodo.841106},
  url    = {https://doi.org/10.5281/zenodo.841106},
}

@Article{igraph,
  author  = {Gabor Csardi and Tamas Nepusz},
  title   = {The igraph software package for complex network research},
  journal = {InterJournal},
  year    = {2006},
  volume  = {Complex Systems},
  pages   = {1695},
  url     = {http://igraph.org},
}

@Manual{shkrdata,
  title  = {shkrdata: Retrieving and handling SHKR data with the package shkrdata},
  author = {Oliver Nakoinz},
  year   = {2018},
  note   = {R package version 0.1.2},
  url    = {https://github.com/ISAAKiel/shkrdata},
}

@Comment{jabref-meta: databaseType:bibtex;}

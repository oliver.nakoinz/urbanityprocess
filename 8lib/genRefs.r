library(bibtex)
library("wrapr")  # get qc() definition

write.bib(qc(R,
             igraph,
             shkrdata
), file='8lib/lit_packages')

